from django.contrib.auth.models import User
from rest_framework import serializers
from lyrical_app.models import SongLyric

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class SongLyricSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SongLyric
        fields = ('id', 'song_name', 'artist_name', 'album_name', 'lyrics')


# class FetchLyricSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = SongLyric
#         fields = ('id', 'song_name', 'artist_name', 'album_name', 'lyrics')
