from django.db import models

# Create your models here.
class SongLyric(models.Model):
    """
    Song lyric model
    """

    # Fields
    song_name = models.CharField(max_length=200, help_text="Song name")
    artist_name = models.CharField(max_length=200, help_text="Artist name")
    album_name = models.CharField(max_length=200, help_text="Album name")
    spotifyID = models.CharField(unique=True, default="0", max_length=200, help_text="Unique Spotify ID")
    lyrics = models.TextField(help_text="Lyrics")

    class Meta:
        ordering = ["song_name", "artist_name"]

    def __repr__(self):
        return "<SongLyric {}>".format(self.song_name)
