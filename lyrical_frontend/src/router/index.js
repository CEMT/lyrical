import Vue from 'vue'
import Router from 'vue-router'
import Lyrics from '@/components/Lyrics'
import Callback from '@/components/Callback'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Lyrics',
      component: Lyrics
    },
    {
      path: '/callback',
      name: 'Callback',
      component: Callback
    }
  ]
})
