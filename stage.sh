#!/bin/bash
source .env
docker start "$POSTGRES_DB"
status=$?
if [ $status != 0 ]; then
    echo "Couldn't start the container - likely does not exist. Recreating. You will need to seed the DB."
    docker run --name "$POSTGRES_DB" -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e POSTGRES_DB=$POSTGRES_DB -d -p 5432:$POSTGRES_PORT postgres
    python manage.py migrate
fi