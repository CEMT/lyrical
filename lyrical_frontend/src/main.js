// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {
  library
} from '@fortawesome/fontawesome-svg-core'
import {
  faSync
} from '@fortawesome/free-solid-svg-icons'
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

library.add(faSync)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

/* eslint-disable no-new */
export const bus = new Vue()
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
