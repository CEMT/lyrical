# Lyrical
This application provides a backend server running Flask with a single set of entry points which will look up lyrics for the users currently playing song on spotify, and, where possible, the lyrics of the next song on the playlist

## Environment variables and files missing from this repository
This application depends on the following environment variables being set in file: lyrical_project/.env
### Django config
SECRET_KEY=

DEBUG=FALSE
#### development, staging, testing, production are the options here.
APP_SETTINGS=production

### Frontend hostname
FRONTEND_HOST=
FRONTEND_PORT=
#### This should be FRONTEND_HOST:FRONTEND_PORT
FRONTEND=

### Backend hostname
BACKEND_HOST=
BACKEND_PORT=
#### This should be BACKEND_HOST:BACKEND_PORT
BACKEND=

#### Database config
POSTGRES_USER=
POSTGRES_PASS=

POSTGRES_PORT=

POSTGRES_HOST=

POSTGRES_DB=

#### Spotify App details
SPOTIFY_CLIENT_ID=

SPOTIFY_CLIENT_SECRET=

#### Genius App details
GENIUS_CLIENT_ID=

GENIUS_CLIENT_SECRET=

GENIUS_CLIENT_ACCESS_TOKEN=