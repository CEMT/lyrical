from django.conf.urls import url, include
from rest_framework import routers
from lyrical_app import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'lyrics', views.SongLyricViewSet)
urlpatterns = [
    url(r'^api_v1/', include(router.urls)),
    url(r'^current', views.GetCurrentLyrics, name="current"),
    url(r'^auth/refresh', views.RefreshSpotifyToken, name="refreshauth"),
    url(r'^auth', views.GetSpotifyToken, name="auth"),
    # url(r'^', views.GetCurrentLyrics, name="index"),
]
