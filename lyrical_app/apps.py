from django.apps import AppConfig


class LyricalAppConfig(AppConfig):
    name = 'lyrical_app'
