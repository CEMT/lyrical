from .base import *

ALLOWED_HOSTS = ['home.cemt.co.uk', 'localhost', 'localhost.cemt.co.uk', '127.0.0.1']
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'Deny'
SECURE_SSL_REDIRECT = False
DEBUG = True
print(DEBUG)
