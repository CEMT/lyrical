#!/usr/bin/env python
import os
import sys
from dotenv import load_dotenv
from pathlib import Path

if __name__ == "__main__":
    load_dotenv(dotenv_path=Path('.') / '.env')
    if 'test' in sys.argv:
        os.environ['APP_SETTINGS'] = "testing"

    app_mode = os.environ['APP_SETTINGS']
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.{}".format(app_mode))
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
