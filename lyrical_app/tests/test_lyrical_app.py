import unittest
import os
import json
from django.test import TestCase, Client
from lyrical_app.models import SongLyric
import pdb

class SonglyricTestCase(TestCase):
    """This class represents a lyric test case"""

    def setUp(self):
        """Define test variables and initialize client"""
        self.client = Client()
        self.songlyric = {
            'song_name': 'A Favor House Atlantic',
            'artist_name': 'Coheed and Cambria',
            'album_name': 'In Keeping Secrets of Silent Earth: 3',
            'lyrics': """
            [Verse 1]
            Your eyes tell the stories of a day you wish you could
            Recall the moments that once have
            Retract the footsteps that brought us to this favor
            I wouldn't ask this of you

            [Interlude]
            Good eye, sniper
            Here, I'll shoot, you run

            [Pre - Chorus]
            The words you scribbled on the walls
            With the loss of friends you didn't have
            I'll call you when the time is right

            Are you in or are you out
            For them all to know the end of us all?

            [Verse 2]
            Run quick, they're behind us, didn't think we'd ever make it
            This close to safety, in one piece
            Now you wanna kill me in the act of what could maybe
            Save us from sleep and what we are

            [Interlude]
            Good eye, sniper
            Now I shoot, you run

            [Pre - Chorus]
            The words you scribbled on the walls
            With the loss of friends you didn't have
            I'll call you when the time is right

            Are you in or are you out
            For them all to know the end of us all?

            [Chorus]
            Bye bye, beautiful
            Don't bother to write

            Disturbed by your words
            And they're calling all cars
            Face step, let down
            Face step, step down

            [Pre - Chorus]
            The words you scribbled on the walls
            The loss of friends you didn't have
            I'll call you when the time is right

            Are you in or are you out
            For them all to know...

            [Chorus]
            Bye bye, beautiful
            Don't bother to write

            Disturbed by your words
            And they're calling all cars
            Face step, let down
            Face step, step down

            Bye bye, beautiful
            Don't bother to write

            Disturbed by your words
            And they're calling all cars
            Face step, let down
            Face step, step down
            """
        }

    def test_songlyric_creation(self):
        """[POST] Test API can create a song lyric"""
        res = self.client.post('/lyrics/', data=self.songlyric)
        self.assertEqual(res.status_code, 201)
        self.assertIn('A Favor House Atlantic', str(res.data))

    def test_songlyric_name_length_limit(self):
        longNameLyric = self.songlyric
        # Set the song lyric song_name attribute to a 201 character random string
        longNameLyric['song_name']=os.urandom(201).hex()
        res = self.client.post('/lyrics/', data=longNameLyric)
        self.assertEqual(res.status_code, 400)


    def test_api_can_get_all_lyrics(self):
        """[GET] Test API can get all lyrics"""
        res = self.client.post('/lyrics/', data=self.songlyric)
        self.assertEqual(res.status_code, 201)
        res = self.client.get('/lyrics/')
        self.assertEqual(res.status_code, 200)
        self.assertIn('A Favor House Atlantic', str(res.data))

    def test_api_can_get_lyric_by_id(self):
        rv = self.client.post('/lyrics/', data=self.songlyric)
        self.assertEqual(rv.status_code, 201)
        created_id = rv.json()['id']
        result = self.client.get('/lyrics/{}/'.format(created_id))
        self.assertEqual(result.status_code, 200)
        self.assertIn('A Favor House Atlantic', str(result.data))

    def test_songlyric_can_be_edited(self):
        """[PUT] Test API can edit an existing lyric"""
        putCreateData = self.client.post(
            '/lyrics/',
            data={
                'song_name': 'We built this city',
                'artist_name': 'Starship',
                'album_name': 'The Waaa',
                'lyrics': 'Tony played the bongos, listen on the radio'
            }
        )
        self.assertEqual(putCreateData.status_code, 201)
        created_id = putCreateData.json()['id']
        updatedData = {
            'song_name': 'We built this city on rock and roll',
            'artist_name': 'Starship',
            'album_name': 'The Waaa',
            'lyrics': 'Tony played the bongos, listen on the radio',
        }
        # The data must be encoded as json otherwise we get a 415 error. This would always be the case from an application
        putChangeData = self.client.put(
            '/lyrics/{}/'.format(created_id),
            data=json.dumps(updatedData), content_type='application/json'
        )
        self.assertEqual(putChangeData.status_code, 200)
        putResult = self.client.get('/lyrics/{}/'.format(created_id))
        self.assertIn('We built this city on rock and roll', str(putResult.data))

    def test_lyric_deletion(self):
        """[DELETE] Test API can delete an existing lyric"""
        deleteCreateData = self.client.post(
            '/lyrics/',
            data={
                'song_name': 'This is it!',
                'artist_name': 'Michael Jackson',
                'album_name': 'This is it',
                'lyrics': 'This is it! Here I stand!'
            }
        )
        self.assertEqual(deleteCreateData.status_code, 201)
        created_id = deleteCreateData.json()['id']
        deleteRequestData = self.client.delete('/lyrics/{}/'.format(created_id))
        self.assertEqual(deleteRequestData.status_code, 204)
        # Test to see if it exists - it should not
        deleteResult = self.client.get('/lyrics/{}/'.format(created_id))
        self.assertEqual(deleteResult.status_code, 404)

    # def tearDown(self):
    #     """Teardown all initialized variables"""


if __name__ == "__main__":
    unittest.main()
