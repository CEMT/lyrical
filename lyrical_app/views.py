from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework import mixins
from lyrical_app.models import SongLyric
from lyrical_app.serializers import UserSerializer, SongLyricSerializer
from django.http import JsonResponse
from os import getenv
from bs4 import BeautifulSoup
import requests
import itertools
import base64
# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    """
    API Endpoint allowing user to be viewed or edited
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SongLyricViewSet(viewsets.ModelViewSet):
    """
    API Endpoint allowing user to be viewed or edited
    """
    queryset = SongLyric.objects.all()
    serializer_class = SongLyricSerializer


def GetLyricsFromGenius(song_name, artist_name, album_name, progress, duration):
    print("Hit the genius API")
    geniusAPIURL = "http://api.genius.com/"
    geniusHeader = {"Authorization": "Bearer {}".format(
        getenv('GENIUS_CLIENT_ACCESS_TOKEN'))}
    searchURL = "{}search?q={songName},{artistName}".format(
        geniusAPIURL, songName=song_name, artistName=artist_name)
    geniusSearchJSON = (requests.get(searchURL, headers=geniusHeader)).json()
    if len(geniusSearchJSON['response']['hits']) == 0:
        print(geniusSearchJSON)
    def getMostViews(geniusSearchJSON):
        for a, b in itertools.combinations(geniusSearchJSON['response']['hits'], 2):
            try:
                if a['result']['title_with_featured'] == song_name or a['result']['title'] == song_name:
                    return a
                elif b['result']['title_with_featured'] == song_name or b['result']['title'] == song_name:
                    return b
                if "translations" not in a['result']['url']:
                    aViews = a['result']['stats']['pageviews']
                else:
                    continue

                if "translations" not in b['result']['url']:
                    bViews = b['result']['stats']['pageviews']
                else:
                    continue

                if aViews and bViews:
                    highest = max(aViews, bViews)
                    print(highest)
                    print("A:{} B:{}".format(aViews,bViews))
                    if highest == aViews:
                        print("a is highest")
                        return a
                    else:
                        print("b is highest")
                        return b
            except KeyError:
                pass

    getResult = getMostViews(geniusSearchJSON)
    try:
        songURL = getResult['result']['url']
    except TypeError as e:
        print("Caught a type-error")
        print(e)
        songURL = geniusSearchJSON['response']['hits'][0]['result']['url']

    songHTML = (requests.get(songURL)).text
    soup = BeautifulSoup(songHTML, 'html.parser')
    lyricsText = soup.find('div',class_="lyrics").get_text()
    songDetails = {
        'song_name':song_name,
        'artist_name':artist_name,
        'album_name':album_name,
        'lyrics':lyricsText,
        'progress': progress,
        'duration': duration,
        'status':'success',
        'status_message':'Retrieved lyrics from external providers for requested song',
    }
    return songDetails

def GetCurrentLyrics(request):
    # Set data required for the request
    userAuth = request.META['HTTP_AUTHORIZATION']
    authHeader = {"Authorization":"Bearer {}".format(userAuth)}

    # Perform the request
    currentTrack = (requests.get("https://api.spotify.com/v1/me/player/currently-playing", headers=authHeader))
    # Check for a 204 return code - this would be returned when the user does not have a song playing
    if currentTrack.status_code == 204:
        responseDict = {
            'status':'failed',
            'status_message':'No track playing'
        }
        return JsonResponse(responseDict, status=204)

    currentTrackJSON = currentTrack.json()
    # Check current track JSON for an error - this would be where invalid auth tokens are returned
    # import pdb; pdb.set_trace()
    if 'error' in currentTrackJSON.keys():
        print(currentTrackJSON)
        responseDict = {
            'status':'failed',
            'status_message':currentTrackJSON['error']['message']
        }
        return JsonResponse(responseDict, status=401)

    # Gather data from response - if we get here we expect to have data available in currentTrackJSON
    currentSongName = currentTrackJSON['item']['name']
    currentArtistName = currentTrackJSON['item']['artists'][0]['name']
    currentAlbumName = currentTrackJSON['item']['album']['name']
    currentProgress = currentTrackJSON['progress_ms']
    currentDuration = currentTrackJSON['item']['duration_ms']
    spotifyID = currentTrackJSON['item']['id']
    print("song: {}, artist: {}, album: {}".format(currentSongName, currentArtistName, currentAlbumName))
    try:
        query = SongLyric.objects.filter(spotifyID=spotifyID)
        responseDict = {
            'song_name':query.get().song_name,
            'artist_name':query.get().artist_name,
            'album_name':query.get().album_name,
            'lyrics':query.get().lyrics,
            'progress': currentProgress,
            'duration': currentDuration,
            'status':'success',
            'status_message':'Retrieved lyrics from database for requested song',
        }
        print("Got it from the DB")
    except SongLyric.DoesNotExist:
        print("Didnt find it in DB")
        responseDict = GetLyricsFromGenius(currentSongName, currentArtistName, currentAlbumName, currentProgress, currentDuration)
        newSong = SongLyric.objects.create(song_name=responseDict['song_name'], artist_name=responseDict['artist_name'], album_name=responseDict['album_name'], lyrics=responseDict['lyrics'], spotifyID=spotifyID)
        newSong.save()
        print("Saved to DB.")
    except Exception as e:
        print("didnt catch it properly")
        print(e)
        responseDict = {
            'status':'failed',
            'status_message':'Unable to find lyrics for requested song',
        }
        pass
    return JsonResponse(responseDict)

def GetSpotifyToken(request):
    tokenURL = "https://accounts.spotify.com/api/token"
    userCode = request.META['HTTP_AUTHORIZATION']
    redirect_uri = "http://{}/callback".format(getenv('FRONTEND'))
    clientInfoEncoded = base64.b64encode(bytes("{}:{}".format(getenv('SPOTIFY_CLIENT_ID'), getenv('SPOTIFY_CLIENT_SECRET')), 'utf-8')).decode()
    print("URL: {} | Code: {}".format(redirect_uri, userCode))
    tokenInfo = requests.post(
        tokenURL,
        data={
            "grant_type": "authorization_code",
            "code": userCode,
            "redirect_uri": redirect_uri
        },
        headers={'Authorization':'Basic {}'.format(clientInfoEncoded)}
    )
    print(tokenInfo)
    print(tokenInfo.json())
    return JsonResponse(tokenInfo.json())

def RefreshSpotifyToken(request):
    tokenURL = "https://accounts.spotify.com/api/token"
    refreshToken = request.META['HTTP_AUTHORIZATION']
    # if getenv('')
    redirect_uri = "http://{}/callback".format(getenv('FRONTEND'))
    clientInfoEncoded = base64.b64encode(bytes("{}:{}".format(getenv('SPOTIFY_CLIENT_ID'), getenv('SPOTIFY_CLIENT_SECRET')), 'utf-8')).decode()
    tokenInfo = requests.post(
        tokenURL,
        data={
            "grant_type": "refresh_token",
            "refresh_token": refreshToken
        },
        headers={'Authorization':'Basic {}'.format(clientInfoEncoded)}
    )
    return JsonResponse(tokenInfo.json())
